﻿// Simplified Bumped Specular shader. Differences from regular Bumped Specular one:
// - no Main Color nor Specular Color
// - Alpha Cutoff
 
Shader "Mobile/Alpha" {
Properties {
    _MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
    _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
}
SubShader {
    Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
    LOD 250
   
CGPROGRAM
#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd alphatest:_Cutoff
 
inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed atten)
{
    fixed diff = max (0, dot (s.Normal, lightDir));
   
    fixed4 c;
    c.rgb = (s.Albedo * _LightColor0.rgb * diff) * (atten);
    c.a = 0.0;

    return c;
}
 
sampler2D _MainTex;
 
struct Input {
    float2 uv_MainTex;
};
 
void surf (Input IN, inout SurfaceOutput o) {
    fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
    o.Albedo = tex.rgb;
    o.Alpha = tex.a;
}
ENDCG
}
 
FallBack "Mobile/Diffuse"
//FallBack "Transparent/Coutout/Diffuse"
}