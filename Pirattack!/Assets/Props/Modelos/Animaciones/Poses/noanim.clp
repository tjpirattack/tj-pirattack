# CAT Character Animation Clip exported from 3dsmax on Wed Mar 09 10:49:18 2016

Version = 3500
LengthAxis = 0
CharacterName = "Hancock"
ClipLength = 3200
LayerRange = Interval 0 0
Units = 0.0237194
PathNodeGuess = matrix -5.11435e-014 1.62921e-007 1 1.50996e-007 -1 1.62921e-007 1 1.50996e-007 2.65431e-014 0 0 -5.73365
BodyPartID = "Hub"
Hub {
  # HancockPelvis
  GroupWeights {
    ValFloat = 1
  }
  Controller {
    ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
    Flags = 448
    OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
    SimpleKeys {
      ValKeyMatrix = KeyMatrix 0 1 2.37e-014 -3.13916e-007 1 -8.74228e-008 -1 -3.13916e-007 1 -8.74228e-008 -5.11435e-014 -1.24345e-013 0.2062 -3.48727
    }
    SubNumber = 0
    Controller {
      ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
      SubNumber = 0
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -1.24345e-013
      }
      SubNumber = 1
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 0.2062
      }
      SubNumber = 2
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -3.48727
      }
    }
    SubNumber = 1
    Controller {
      ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
      AxisOrder = 0
      SubNumber = 0
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -1.5708
      }
      SubNumber = 1
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -1.5708
      }
      SubNumber = 2
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -1.5708
      }
    }
    SubNumber = 2
    Controller {
      ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
      SubNumber = 0
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 1
      }
      SubNumber = 1
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 1
      }
      SubNumber = 2
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 1
      }
    }
  }
  Limb {
    # LLeg
    LimbFlags = 4108
    GroupWeights {
      ValFloat = 1
    }
    IKFKValues {
      ValFloat = 0
    }
    LimbIKPos {
      ValFloat = 3
    }
    Retargetting {
      ValFloat = 0
    }
    IKTargetValues {
      Controller {
        ClassIDs = ClassID 36872 1848908986 1872515786 "CATHDPivotTrans"
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 2.37e-014 -3.13916e-007 1 -8.74228e-008 -1 -3.13916e-007 1 -8.74228e-008 -5.11435e-014 0.474387 0.2062 -5.73365
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
          Flags = 448
          OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = 0.474387
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = 0.2062
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -5.73365
            }
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
            AxisOrder = 0
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -1.5708
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -1.5708
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -1.5708
            }
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36877 8208 0 "Bezier Scale"
            ValPoint = point 1 1 1
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36875 8200 0 "Bezier Position"
          ValPoint = point 0 0 0
        }
        SubNumber = 2
        ParamBlock {
        }
      }
    }
    LimbBone {
      # HancockRigLLeg1
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.999101 0.0292392 -0.0306913 -0.0303741 0.998847 -0.0371863 0.0295686 0.038085 0.998837 0.37756 -4.79817e-006 -0.599505
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.37756
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -4.79817e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.599505
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0372124
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0306961
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0292572
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigLLeg1
      }
    }
    LimbBone {
      # HancockRigLLeg2
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.999424 -0.0336027 0.0049877 0.0330373 0.995609 0.0875914 -0.00790909 -0.087376 0.996144 -2.38419e-006 -2.95043e-006 6.55651e-007
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -2.38419e-006
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -2.95043e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 6.55651e-007
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0877049
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.00498772
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0336094
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigLLeg2
      }
    }
    Palm {
      LMR = -1
      GroupWeights {
        ValFloat = 1
      }
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 -0.0319902 0.999483 0.00312566 -0.999488 -0.0319904 0 9.99911e-005 -0.00312406 0.999995 -0.25714 0.379032 -0.169348
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.25714
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.379032
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.169348
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.00312567
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1.60279
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      TargetAlignValues {
        ValFloat = 1
      }
    }
  }
  Limb {
    # RLeg
    LimbFlags = 4132
    GroupWeights {
      ValFloat = 1
    }
    IKFKValues {
      ValFloat = 0
    }
    LimbIKPos {
      ValFloat = 3
    }
    Retargetting {
      ValFloat = 0
    }
    IKTargetValues {
      Controller {
        ClassIDs = ClassID 36872 1848908986 1872515786 "CATHDPivotTrans"
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 2.37e-014 -3.13916e-007 1 -8.74228e-008 -1 -3.13916e-007 1 -8.74228e-008 -5.11435e-014 -0.474388 0.206199 -5.73365
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
          Flags = 448
          OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -0.474388
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = 0.206199
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -5.73365
            }
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
            AxisOrder = 0
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -1.5708
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -1.5708
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ValFloat = -1.5708
            }
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36877 8208 0 "Bezier Scale"
            ValPoint = point 1 1 1
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36875 8200 0 "Bezier Position"
          ValPoint = point 0 0 0
        }
        SubNumber = 2
        ParamBlock {
        }
      }
    }
    LimbBone {
      # HancockRigRLeg1
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.999101 0.0292394 0.0306916 -0.0303742 0.998847 0.0371866 -0.0295689 -0.0380854 0.998837 0.37756 -4.79817e-006 0.599505
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.37756
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -4.79817e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.599505
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0372127
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0306964
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0292573
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigRLeg1
      }
    }
    LimbBone {
      # HancockRigRLeg2
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.999422 -0.033542 -0.00567426 0.0329169 0.995612 -0.0875913 0.00858734 0.0873538 0.99614 -2.86102e-006 -2.92063e-006 -6.25849e-007
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -2.86102e-006
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -2.92063e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -6.25849e-007
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0877051
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.00567428
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0335488
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.999999
          }
        }
      }
      BoneSeg {
        # HancockRigRLeg2
      }
    }
    Palm {
      LMR = 1
      GroupWeights {
        ValFloat = 1
      }
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 -0.0320361 0.999485 -0.00158213 -0.999487 -0.0320361 1.04716e-007 -5.05806e-005 0.00158132 0.999999 -0.257144 0.379102 0.169406
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.257144
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.379102
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.169406
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1.04716e-007
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.00158213
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1.60284
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      TargetAlignValues {
        ValFloat = 1
      }
    }
  }
}

