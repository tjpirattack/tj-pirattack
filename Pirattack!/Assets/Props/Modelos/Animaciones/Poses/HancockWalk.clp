# CAT Character Animation Clip exported from 3dsmax on Wed Mar 09 12:32:50 2016

Version = 3500
LengthAxis = 0
CharacterName = "Hancock"
ClipLength = 4000
LayerRange = Interval 0 0
Units = 0.0237194
PathNodeGuess = matrix 1.95467e-008 -7.28988e-010 1 -0.0373871 -0.999301 2.30216e-012 0.999301 -0.0373871 -1.95603e-008 -0.00034184 6.76885e-006 -5.73365
BodyPartID = "Hub"
Hub {
  # HancockPelvis
  GroupWeights {
    ValFloat = 1
  }
  Controller {
    ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
    Flags = 448
    OffsetTM = matrix 1 -8.74228e-008 -8.74228e-008 8.74228e-008 1 -8.74228e-008 8.74228e-008 8.74228e-008 1 0 0 0
    SimpleKeys {
      ValKeyMatrix = KeyMatrix 0 1 1.58168e-012 -4.33126e-007 1 -0.0871557 -0.996195 -4.31477e-007 0.996195 -0.0871557 -3.7751e-008 -1.24678e-013 0.00914241 -3.43068
      ValKeyMatrix = KeyMatrix 0 1120 0.0345017 -3.12582e-007 0.999405 0.0586385 -0.998277 -0.00202464 0.997683 0.0586734 -0.0344422 -1.24678e-013 0.00914241 -3.43068
      ValKeyMatrix = KeyMatrix 0 2080 2.37e-014 -3.13916e-007 1 0.173648 -0.984808 -3.09147e-007 0.984808 0.173648 5.4511e-008 -1.24678e-013 0.00914241 -3.43068
      ValKeyMatrix = KeyMatrix 0 3200 -0.0219917 -3.12671e-007 0.999758 0.0285483 -0.999592 0.000627666 0.99935 0.0285552 0.0219827 -1.24678e-013 0.00914241 -3.43068
      ValKeyMatrix = KeyMatrix 0 3999 -0.00019363 -0.00163721 0.999999 -0.0785052 -0.996912 -0.00164736 0.996914 -0.0785054 6.45024e-005 -1.24678e-013 0.00914241 -3.43068
    }
    SubNumber = 0
    Controller {
      ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
      SubNumber = 0
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -1.24678e-013
      }
      SubNumber = 1
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 0.00914241
      }
      SubNumber = 2
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = -3.43068
      }
    }
    SubNumber = 1
    Controller {
      ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
      AxisOrder = 0
      SubNumber = 0
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ORTIn = 1
        ORTOut = 1
        RangeStart = 0
        RangeEnd = 4160
        Keys {
          ValKeyBezFloat = KeyBezFloat 6912 0 -1.65806 0 0 -1 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 1120 -3.08288 -0 0 0.3333 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 2080 -1.39626 -0.00149588 0.00149588 0.3333 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 3200 0.0285449 -0 0 0.3333 0.3333
        }
      }
      SubNumber = 1
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ORTIn = 1
        ORTOut = 1
        RangeStart = 0
        RangeEnd = 4160
        Keys {
          ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 1120 -1.53629 -0 0 0.3333 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 2080 -1.5708 -0 0 0.3333 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 3200 -1.5488 -0 0 0.3333 0.3333
        }
      }
      SubNumber = 2
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ORTIn = 1
        ORTOut = 1
        RangeStart = 0
        RangeEnd = 4160
        Keys {
          ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 1120 -9.05991e-006 -0 0 0.3333 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 2080 -1.5708 0.00151037 -0.00151037 0.3333 0.3333
          ValKeyBezFloat = KeyBezFloat 6912 3200 -3.14158 -0 0 0.3333 0.3333
        }
      }
    }
    SubNumber = 2
    Controller {
      ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
      SubNumber = 0
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 1
      }
      SubNumber = 1
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 1
      }
      SubNumber = 2
      Controller {
        ClassIDs = ClassID 36867 8199 0 "Bezier Float"
        ValFloat = 1
      }
    }
  }
  Limb {
    # LLeg
    LimbFlags = 4108
    GroupWeights {
      ValFloat = 1
    }
    IKFKValues {
      ValFloat = 0
    }
    LimbIKPos {
      ValFloat = 3
    }
    Retargetting {
      ValFloat = 0
    }
    IKTargetValues {
      Controller {
        ClassIDs = ClassID 36872 1848908986 1872515786 "CATHDPivotTrans"
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 2.37e-014 -3.13916e-007 1 -8.74228e-008 -1 -3.13916e-007 1 -8.74228e-008 -5.11435e-014 0.701268 -0.820065 -5.74669
          ValKeyMatrix = KeyMatrix 0 1760 -5.40356e-009 0.0715724 0.997435 -8.76164e-008 -0.997435 0.0715724 1 -8.7005e-008 1.16606e-008 0.701268 0.259907 -5.72803
          ValKeyMatrix = KeyMatrix 0 2080 -6.97355e-009 0.0923674 0.995725 -8.77455e-008 -0.995725 0.0923674 1 -8.67263e-008 1.50486e-008 0.701268 0.628462 -5.6044
          ValKeyMatrix = KeyMatrix 0 3200 -1.17584e-008 0.155745 0.987797 -8.83441e-008 -0.987797 0.155745 1 -8.54347e-008 2.5374e-008 0.701268 -0.522998 -5.53542
          ValKeyMatrix = KeyMatrix 0 4000 -1.311e-008 0.173648 0.984808 -8.85698e-008 -0.984808 0.173648 1 -8.49477e-008 2.82908e-008 0.701268 -0.853264 -5.63613
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
          Flags = 448
          OffsetTM = matrix 1 -8.74228e-008 -8.74228e-008 8.74228e-008 1 -8.74279e-008 8.74228e-008 8.74279e-008 1 0 0 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4160
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 0.701268 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1760 0.701268 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 0.701268 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 3200 0.701268 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 4000 0.701268 -0 0 0.3333 0.3333
              }
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4160
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -0.820065 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1760 0.259907 -0.000696407 0.000696407 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 0.628462 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 3200 -0.522998 0.000771732 -0.000771732 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 4000 -0.853264 -0 0 0.3333 0.3333
              }
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4160
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -5.74669 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1760 -5.72803 -3.17931e-005 3.17931e-005 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -5.6044 -0.000133757 0.000133757 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 3200 -5.53542 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 4000 -5.63613 0.000220065 -0.000220065 0.3333 0.3333
              }
            }
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
            AxisOrder = 0
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4000
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 4000 -1.5708 0 0 0.3333 -1
              }
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4000
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 4000 -1.74533 0 0 0.3333 -1
              }
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4000
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 4000 -1.5708 0 0 0.3333 -1
              }
            }
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36877 8208 0 "Bezier Scale"
            ValPoint = point 1 1 1
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36875 8200 0 "Bezier Position"
          ValPoint = point 0 0 0
        }
        SubNumber = 2
        ParamBlock {
        }
      }
    }
    LimbBone {
      # HancockRigLLeg1
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.984149 0.113313 -0.136421 -0.117313 0.99286 -0.0216238 0.132997 0.037285 0.990415 0.37756 -4.79817e-006 -0.599506
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.37756
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -4.79817e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.599506
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0218296
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.136848
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.114633
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigLLeg1
      }
    }
    LimbBone {
      # HancockRigLLeg2
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.866851 -0.47134 0.162503 0.453771 0.88091 0.134496 -0.206544 -0.0428488 0.977499 -4.76837e-006 -3.06964e-006 1.78814e-007
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -4.76837e-006
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -3.06964e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1.78814e-007
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.136734
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.163227
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.498023
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.999999
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigLLeg2
      }
    }
    Palm {
      LMR = -1
      GroupWeights {
        ValFloat = 1
      }
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 -0.0321507 0.999479 0.00267028 -0.999483 -0.0321508 -4.76835e-007 8.53753e-005 -0.00266892 0.999996 -0.257286 -0.038032 0.0250587
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.257286
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.038032
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0250587
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -4.76837e-007
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.00267029
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1.60295
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      TargetAlignValues {
        ValFloat = 1
      }
    }
  }
  Limb {
    # RLeg
    LimbFlags = 4132
    GroupWeights {
      ValFloat = 1
    }
    IKFKValues {
      ValFloat = 0
    }
    LimbIKPos {
      ValFloat = 3
    }
    Retargetting {
      ValFloat = 0
    }
    IKTargetValues {
      Controller {
        ClassIDs = ClassID 36872 1848908986 1872515786 "CATHDPivotTrans"
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 1.47e-014 -1.94707e-007 1 -8.74228e-008 -1 -1.94707e-007 1 -8.74228e-008 -3.17218e-014 -0.665909 0.283819 -5.70234
          ValKeyMatrix = KeyMatrix 0 1280 -9.73345e-009 0.128924 0.991655 -8.80529e-008 -0.991655 0.128924 1 -8.60631e-008 2.10043e-008 -0.665909 -0.717357 -5.53205
          ValKeyMatrix = KeyMatrix 0 1920 -1.311e-008 0.173648 0.984808 -8.85698e-008 -0.984808 0.173648 1 -8.49477e-008 2.82908e-008 -0.665909 -0.87437 -5.68463
          ValKeyMatrix = KeyMatrix 0 2080 2.37e-014 -3.13916e-007 1 -8.74228e-008 -1 -3.13916e-007 1 -8.74228e-008 -5.11435e-014 -0.665909 -0.876654 -5.70234
          ValKeyMatrix = KeyMatrix 0 3999 2.37e-014 -3.13916e-007 1 -8.74228e-008 -1 -3.13916e-007 1 -8.74228e-008 -5.11435e-014 -0.665909 0.264039 -5.70234
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
          Flags = 448
          OffsetTM = matrix 1 0 0 0 1 -3.63798e-012 0 3.63798e-012 1 0 0 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4160
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -0.665909 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1280 -0.665909 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -0.665909 -0 0 0.3333 0.3333
              }
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4160
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 0.283821 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1280 -0.717357 0.00055792 -0.00055792 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -0.876654 -0 0 0.3333 0.3333
              }
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 4160
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -5.70234 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1280 -5.53205 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -5.70234 -0 0 0.3333 0.3333
              }
            }
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
            AxisOrder = 0
            SubNumber = 0
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 2080
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1920 -1.5708 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -1.5708 0 0 0.3333 -1
              }
            }
            SubNumber = 1
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 2080
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1920 -1.74533 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -1.5708 0 0 0.3333 -1
              }
            }
            SubNumber = 2
            Controller {
              ClassIDs = ClassID 36867 8199 0 "Bezier Float"
              ORTIn = 1
              ORTOut = 1
              RangeStart = 0
              RangeEnd = 2080
              Keys {
                ValKeyBezFloat = KeyBezFloat 6912 0 -1.5708 0 0 -1 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 1920 -1.5708 -0 0 0.3333 0.3333
                ValKeyBezFloat = KeyBezFloat 6912 2080 -1.5708 0 0 0.3333 -1
              }
            }
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36877 8208 0 "Bezier Scale"
            ValPoint = point 1 1 1
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36875 8200 0 "Bezier Position"
          ValPoint = point 0 0 0
        }
        SubNumber = 2
        ParamBlock {
        }
      }
    }
    LimbBone {
      # HancockRigRLeg1
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.993026 0.0378102 0.111671 -0.0402149 0.999004 0.0193597 -0.110827 -0.0237155 0.993557 0.37756 -4.79817e-006 0.599505
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.37756
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -4.79817e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.599505
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0194828
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.111904
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0380574
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigRLeg1
      }
    }
    LimbBone {
      # HancockRigRLeg2
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 0.927252 -0.355803 -0.116634 0.336957 0.928767 -0.154442 0.163277 0.103906 0.981095 -3.8147e-006 -3.08454e-006 -7.7486e-007
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -3.8147e-006
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -3.08454e-006
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -7.7486e-007
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.156137
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.116901
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.366391
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.999998
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      BoneSeg {
        # HancockRigRLeg2
      }
    }
    Palm {
      LMR = 1
      GroupWeights {
        ValFloat = 1
      }
      Controller {
        ClassIDs = ClassID 36872 8197 0 "Position/Rotation/Scale"
        Flags = 448
        OffsetTM = matrix 1 0 0 0 1 0 0 0 1 0 0 0
        SimpleKeys {
          ValKeyMatrix = KeyMatrix 0 1 -0.031961 0.999489 -0.00117155 -0.999489 -0.031961 3.43425e-009 -3.74404e-005 0.00117095 0.999999 -0.257131 -0.0193345 0.0180613
        }
        SubNumber = 0
        Controller {
          ClassIDs = ClassID 36875 294616578 4293796746 "Position XYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.257131
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = -0.0193345
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.0180613
          }
        }
        SubNumber = 1
        Controller {
          ClassIDs = ClassID 36876 8210 0 "Euler XYZ"
          AxisOrder = 0
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 3.43425e-009
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 0.00117155
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1.60276
          }
        }
        SubNumber = 2
        Controller {
          ClassIDs = ClassID 36877 294616065 4277019531 "ScaleXYZ"
          SubNumber = 0
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 1
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
          SubNumber = 2
          Controller {
            ClassIDs = ClassID 36867 8199 0 "Bezier Float"
            ValFloat = 1
          }
        }
      }
      TargetAlignValues {
        ValFloat = 1
      }
    }
  }
}

