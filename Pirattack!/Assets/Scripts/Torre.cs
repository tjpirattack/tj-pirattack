﻿using UnityEngine;
using System.Collections;

public class Torre : Personaje
{
   [SerializeField]
   private Animation _anim;

   [SerializeField]
   private GameObject _particles;

   [SerializeField]
   private GameObject _fireParticles;

   [SerializeField]
   private GameObject _mechaParticles;

   [SerializeField]
   private Projectile _projectile;

   [SerializeField]
   private AudioSource _explosion;

   [SerializeField]
   private Pirata.PirateKind _characterWin;

   void OnTriggerEnter(Collider i_other)
   {
      if(_anim)
         _anim.Play();
      if(_particles)
         _particles.SetActive(true);
      if(_fireParticles)
         _fireParticles.SetActive(true);
      if(_mechaParticles)
         _mechaParticles.SetActive(false);

      //Peloton
      if(GetComponent<Peloton>())
      {
         GetComponent<Peloton>().Shot();
      }

      if(_characterWin == Pirata.PirateKind.Fat || _characterWin == Pirata.PirateKind.Bombs) 
         Attack();
      else
         Invoke("Attack", 1f);
   }

   void Attack()
   {
      //Point to captain
      Pirata nearestPirate = TropaController.Instance._piratas[4];

      //If exists a pirate nearest than the captain, point at him 
      foreach(Pirata p in TropaController.Instance._piratas)
      {
         if(p._kind != Pirata.PirateKind.Captain 
            && Vector3.Distance(transform.position, p.gameObject.transform.position) 
            < Vector3.Distance(transform.position, nearestPirate.gameObject.transform.position))
         {
            nearestPirate = p;
         }
      }
      if(nearestPirate._isDead)
      {
         nearestPirate = TropaController.Instance._piratas[4];
      }

      _projectile.gameObject.SetActive(true);
      _projectile._pirateTarget = nearestPirate.gameObject;
      _projectile.enabled = true;
      _projectile._characterWin = _characterWin;
      if(_explosion)
         _explosion.Play();
   }
}
