﻿using UnityEngine;
using System.Collections;

public class VerticalPiece : Piece
{
	[SerializeField]
	private GameObject	_leftPoint;

	[SerializeField]
	private GameObject	_rightPoint;

	[SerializeField]
	private Torre	_leftTower;

	[SerializeField]
	private Torre	_rightTower;

	void Awake()
	{
		if (_leftTower != null) {
			GameObject go = (GameObject) GameObject.Instantiate (_leftTower.gameObject, _leftPoint.transform.position, new Quaternion (0, 0, 0, 0));
			go.transform.Rotate(new Vector3(0, -90,0));
		}
		if (_rightTower != null) {
			GameObject go = (GameObject) GameObject.Instantiate (_rightTower.gameObject, _rightPoint.transform.position, new Quaternion (0, 0, 0, 0));
			go.transform.Rotate(new Vector3(0, 90,0));
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
