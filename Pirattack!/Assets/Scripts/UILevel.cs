﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UILevel : Singleton<UILevel>
{
   [SerializeField]
   private GameObject _failText;
   
   [SerializeField]
   private GameObject _winText;


   public void LoadLevel(int i_level)
   {
      SceneManager.LoadScene(i_level);
   }

   public void WinLevel()
   {
      _winText.SetActive(true);
      
	  StartCoroutine (Co_Wait ());
   }

   public void FailLevel()
   {
      _failText.SetActive(true);

	  StartCoroutine (Co_Wait ());
   }

   public void ResetLevel()
   {
      SceneManager.LoadScene(SceneManager.GetActiveScene().name);
   }

	IEnumerator Co_Wait()
	{
		yield return new WaitForSeconds (3.0f);

		SceneManager.LoadScene ("Menu");
	}
}
