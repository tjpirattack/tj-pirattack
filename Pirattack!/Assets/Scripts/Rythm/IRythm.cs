﻿using System;

/// <summary>
/// Interfaz de comunicación el RythmGenerator y cualqueir otro objeto
/// </summary>
public interface IRythm
{
	void Pulse();
}