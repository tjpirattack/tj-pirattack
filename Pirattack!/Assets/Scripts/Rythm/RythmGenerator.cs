﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Esta clase genera un ritmo acorde a una frecuencia
/// </summary>
public class RythmGenerator: MonoBehaviour
{
	private float _frequency;
	private System.Collections.Generic.List<IRythm> _listeners;

    void Start()
    {
       
    }
   public void ComeOn()
   {
      StartCoroutine(GenerateRythm());
   }
    public RythmGenerator ()
	{
        this._frequency = 2f;
	}

    /// <summary>
    /// Configura la frecuencia (segundos) con la que se va agenerar un pulso
    /// </summary>
    /// <param name="frequency"></param>
	public void ConfigureFrequency(float frequency)
	{
		this._frequency = frequency;
	}

    /// <summary>
    /// Añade un listener al generador, cada vez que se genere un pulso este será notificado
    /// a toda la lista de listeners subscritos
    /// </summary>
    /// <param name="listener"></param>
	public void AddListeners(IRythm listener)
	{
		if (this._listeners == null) {
			_listeners = new System.Collections.Generic.List<IRythm>();
		}

		this._listeners.Add (listener);
	}

    /// <summary>
    /// Elimina un listener de la lista, cuidado, el método equals debe estar correctamente implementado
    /// </summary>
    /// <param name="removable"></param>
	public void RemoveListener(IRythm removable)
	{
		foreach (IRythm listener in this._listeners) {
			if (removable.Equals (listener))
			{
				this._listeners.Remove (removable);
			}
		}
	}

	public void Clear()
	{
		this._listeners.Clear ();
	}

    private IEnumerator GenerateRythm()
    {
        for (;;)
        {
            if (this._listeners != null)
            {
                Notify();
            }
            yield return new WaitForSeconds(this._frequency);
        }
    }

	private void Notify()
	{
		foreach (IRythm listener in this._listeners) {
	    	listener.Pulse ();
		}
	}
		
}


