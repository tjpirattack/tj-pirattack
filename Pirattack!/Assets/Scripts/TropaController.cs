﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TropaController : Singleton<TropaController>
{
   [SerializeField]
   private PuntoLista _puntoInicial;

   [SerializeField]
   private GameObject _flechaIzq;
   [SerializeField]
   private GameObject _flechaDer;
   [SerializeField]
   private GameObject _flechaDefault;
   [SerializeField]
   private GameObject _botonIzq;
   [SerializeField]
   private GameObject _botonDer;
   [SerializeField]
   private AudioClip[]    _clipsCaptain;
   
   public Transform[] _posiciones;
   public Pirata[] _piratas;
   public float   _velocidad = 2;
   
   private float p_velocidad2;
   private AudioSource p_audioSource;
   private Pirata p_captain;
   private Animator animator;
   private bool p_walking = true;

   // Use this for initialization
   void Start ()
   {
      _piratas = GetComponentsInChildren<Pirata>();
      p_captain = _piratas[4];
      p_audioSource = GetComponent<AudioSource>();
      Reinicio();
      animator = _piratas[4].GetComponentInChildren<Animator>();

      //Enable walk animation
      foreach(Pirata p in _piratas)
      {
         p.GetComponentInChildren<Animator>().SetBool("idle", false);
         p.GetComponentInChildren<Animator>().SetBool("dying", false);
         p.GetComponentInChildren<Animator>().SetBool("signal1", false);
         p.GetComponentInChildren<Animator>().SetBool("walking", true);
      }
   }
	
	// Update is called once per frame
	void Update ()
   {
      if (Tutorial.Instance._active)
         return;

      p_velocidad2 = _velocidad * Time.deltaTime;
      transform.position = Vector3.MoveTowards(transform.position, _puntoInicial.transform.position, p_velocidad2);

      if(transform.position.Equals(_puntoInicial.transform.position))
      {
         if (_puntoInicial._siguientePunto != null)
         {
            StartCoroutine(Rotate());
            _puntoInicial = _puntoInicial._siguientePunto;
            Reinicio();
         }
      }
      Reinicio();
      //Disable arrows if not pulse
      if (!p_captain.pulse)
      {
         _botonDer.SetActive(false);
         _botonIzq.SetActive(false);
      }
   }

   IEnumerator Rotate ()
   {
      //Debug.Log(_puntoInicial.transform.position.x+ "         "+ _puntoInicial._siguientePunto.transform.position.x);
      if (_puntoInicial.transform.position.x > _puntoInicial._siguientePunto.transform.position.x)
      {
         for (int i = 0; i < 5; i++)
         {
            if (_piratas[i]._isDead == false)
            {
               _piratas[i].transform.rotation = Quaternion.Euler(0, -90, 0);
            }
         }
      }
      else if (_puntoInicial.transform.position.x < _puntoInicial._siguientePunto.transform.position.x)
      {
         for (int i = 0; i < 5; i++)
         {
            if (_piratas[i]._isDead == false)
            {
               _piratas[i].transform.rotation = Quaternion.Euler(0, 90, 0);
            }
         }
      }
      else if (_puntoInicial.transform.position.x == _puntoInicial._siguientePunto.transform.position.x)
      {
         for (int i = 0; i < 5; i++)
         {
            if (_piratas[i]._isDead == false)
            {
               _piratas[i].transform.rotation = Quaternion.Euler(0, 0, 0);
            }
         }
      }

      float time = 0.1f;
      while(time > 0)
      {
         for (int i = 0; i < 5; i++)
            _piratas[i].transform.position = new Vector3(_piratas[i].transform.position.x,
               _piratas[i].transform.position.y + 0.2f,
               _piratas[i].transform.position.z);
         yield return new WaitForSeconds(0.05f);
         time -= 0.05f;
      }
      time = 0.1f;
      while (time > 0)
      {
         for (int i = 0; i < 5; i++)
            _piratas[i].transform.position = new Vector3(_piratas[i].transform.position.x,
               _piratas[i].transform.position.y - 0.2f,
               _piratas[i].transform.position.z);
         yield return new WaitForSeconds(0.05f);
         time -= 0.05f;
      }
   }

   void Reinicio()
   {
      _flechaIzq.SetActive(false);
      _flechaDer.SetActive(false);
      _flechaDefault.SetActive(true);
      _botonDer.SetActive(false);
      _botonIzq.SetActive(false);

      if (_puntoInicial._puntoDerecha != null && _puntoInicial._puntoIzquierda!= null)
      {
         _botonDer.SetActive(true);
         _botonIzq.SetActive(true);
      }
   }

   public void Girar(bool i_izquierda)
   {
      if (i_izquierda)
      {
         if (_puntoInicial._puntoIzquierda != null)
         {
            _puntoInicial._siguientePunto = _puntoInicial._puntoIzquierda;
            _flechaDefault.SetActive(false);
            _flechaDer.SetActive(false);
            _flechaIzq.SetActive(true);
         }
      }
      else
      {
         if (_puntoInicial._puntoDerecha != null)
         {
            _puntoInicial._siguientePunto = _puntoInicial._puntoDerecha;
            _flechaDefault.SetActive(false);
            _flechaIzq.SetActive(false);
            _flechaDer.SetActive(true);
         }
      }      
   }

   public void GirarPiratas()
   {
      foreach(Pirata p in _piratas)
      {
         if(p._kind != Pirata.PirateKind.Captain)
         {
            if (p._actualPos == 3)
               p._actualPos = 0;
            else
               p._actualPos++;
            if (p._isDead == false)
            {
               p._targetPos = _posiciones[p._actualPos].gameObject;
               p.enabled = true;
            }
         }
      }
   }

   public void PlayVoice()
   {
      //Play sound and play animation
      if (this.enabled)
      {
         if (p_audioSource != null)
         {
            p_audioSource.clip = _clipsCaptain[Random.Range(0, _clipsCaptain.Length)];
            p_audioSource.Play();
         }
         if (animator != null)
         {
            animator.SetBool("idle", false);
            animator.SetBool("walking", false);
            animator.SetBool("dying", false);

            animator.SetBool("signal1", true);

            StartCoroutine(Co_StopAnimation());
            p_walking = !p_walking;
         }
      }
   }

   IEnumerator Co_StopAnimation()
   {
      yield return new WaitForSeconds(0.5f);
      animator.SetBool("idle", false);
      animator.SetBool("walking", true);
      animator.SetBool("dying", false);
      animator.SetBool("signal1", false);
   }
}
