﻿using UnityEngine;
using System.Collections;

public static class CharacterControllerParamKeys  {

    public static readonly string Speed = "Speed";
    public static readonly string Speedx2 = "Speedx2";

    public static readonly string FrontSpeed = "FrontSpeed";
    public static readonly string Rotation = "Rotation";
    public static readonly string LateralSpeed = "LateralSpeed";

    public static readonly string Jump = "Jump";
    public static readonly string OnAir = "OnAir";
    public static readonly string Aiming = "Aiming";
    public static readonly string Fire = "Fire";
    public static readonly string FireSecondary = "FireSecondary";

    public static readonly string Death = "Death";
    public static readonly string Victory = "Victory";
}

public static class CharacterControllerAniamtionKeys
{
    public static readonly string Melee1stAttackLeft = "weaposableattack1izq";
    public static readonly string Melee2ndAttackLeft = "weaposableattack2izq";
}