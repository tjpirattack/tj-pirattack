﻿using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(Collider))]
public class Pirata : Personaje, IRythm
{
   public enum PirateKind { Fat, Woman, TwoGuns, Bombs, Captain };

   public PirateKind    _kind;
   public int           _actualPos = 0;
   public GameObject    _targetPos;
   public float         _velocity = 4;
   public bool          _isDead = false;
   public bool          _isEspecial = false;

   private float        p_velocity2;
   private Animator animator;

	public bool pulse = false;

   public GameObject[] _prefabs;

   // Use this for initialization
    void Start ()
    {
      if (_kind != PirateKind.Captain)
      {
         GameObject go = GameObject.Instantiate(_prefabs[PlayerPrefs.GetInt(_actualPos.ToString())]);
         go.transform.SetParent(transform);
         go.transform.localPosition = new Vector3(0, 0, 0);
         _kind = (PirateKind) PlayerPrefs.GetInt(_actualPos.ToString());
         _salud = PlayerPrefs.GetInt(_actualPos.ToString()) == 0 ? 2 : 1;
      }
      this.enabled = false;
      RegisterToRythmGenerator();
      animator = GetComponentInChildren<Animator>();

      if (_kind != PirateKind.Woman && _kind != PirateKind.TwoGuns && _kind != PirateKind.Bombs)
      {
         animator.SetBool("idle", true);
         animator.SetBool("walking", false);
         animator.SetBool("signal1", false);
         animator.SetBool("dying", false);
      }
      else
      {
         animator.SetLayerWeight((int)CharacterAnimatorLayers.AbsoluteLayer, 1);
         animator.SetFloat(CharacterControllerParamKeys.FrontSpeed, 0.5f);
         animator.SetFloat(CharacterControllerParamKeys.Speed, 1.0f);
         transform.GetChild(0).localScale = new Vector3(10, 10, 10);
      }
    }
   
    private void RegisterToRythmGenerator()
    {
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        RythmGenerator RythmGenerator = camera.GetComponent<RythmGenerator>();
        RythmGenerator.AddListeners(this);
    }

    // Update is called once per frame
    void Update ()
   {
      p_velocity2 = _velocity * Time.deltaTime;
      transform.position = Vector3.MoveTowards(transform.position, _targetPos.transform.position, p_velocity2);

      if (transform.position.Equals(_targetPos))
      {
         this.enabled = false;
      }
   }

   void OnMouseDown()
   {
      //If not in rythm return
      if(!pulse)
      {
         return;
      }
      switch(_kind)
      {
         case PirateKind.Fat:
            transform.localScale = new Vector3(2, 1, 2);
            if (Tutorial.Instance._active)
            {
               Tutorial.Instance.Bubba();
            }
            break;
         case PirateKind.Woman:
            animator.SetLayerWeight((int)CharacterAnimatorLayers.CarryLongWeapon, 1);
            animator.SetBool(CharacterControllerParamKeys.Aiming, true);
            if(_actualPos == 1)
            {
               transform.rotation = Quaternion.Euler(0,90,0);
            }
            if (_actualPos == 2)
            {
               transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            if (_actualPos == 3)
            {
               transform.rotation = Quaternion.Euler(0, 270, 0);
            }
            break;
         case PirateKind.TwoGuns:
            animator.SetLayerWeight((int)CharacterAnimatorLayers.CarryShortWeaponLeft, 1);
            animator.SetBool(CharacterControllerParamKeys.Aiming, true);
            if (_actualPos == 1)
            {
               transform.rotation = Quaternion.Euler(0, 90, 0);
            }
            if (_actualPos == 2)
            {
               transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            if (_actualPos == 3)
            {
               transform.rotation = Quaternion.Euler(0, 270, 0);
            }
            break;
         case PirateKind.Bombs:
            GetComponentInChildren<ProjectileChamaco>()._smog.SetActive(true);
            break;
         case PirateKind.Captain:
            TropaController.Instance.GirarPiratas();
            Tutorial.Instance.enabled = true;
            if(Tutorial.Instance._active)
            {
               GetComponent<Collider>().enabled = false;
            }
            break;
      }
      StartCoroutine("Co_Special");
      _isEspecial = true;
   }

   IEnumerator Co_Special()
   {
      yield return new WaitForSeconds(2.0f);
      switch (_kind)
      {
         case PirateKind.Fat:
            transform.localScale = new Vector3(1, 1, 1);
            break;
         case PirateKind.Bombs:
            animator.SetLayerWeight((int)CharacterAnimatorLayers.CarryMeleeWeaponLeft, 0);
            animator.SetLayerWeight((int)CharacterAnimatorLayers.AbsoluteLayer, 1);
            animator.SetFloat(CharacterControllerParamKeys.FrontSpeed, 0.5f);
            animator.SetFloat(CharacterControllerParamKeys.Speed, 1.0f);
            GetComponentInChildren<ProjectileChamaco>()._smog.SetActive(false);
            break;
         case PirateKind.Woman:
            animator.SetLayerWeight((int)CharacterAnimatorLayers.CarryLongWeapon, 0);
            animator.SetLayerWeight((int)CharacterAnimatorLayers.AbsoluteLayer, 1);
            animator.SetFloat(CharacterControllerParamKeys.FrontSpeed, 0.5f);
            animator.SetFloat(CharacterControllerParamKeys.Speed, 1.0f);
            transform.localScale = new Vector3(1, 1, 1);
            transform.rotation = Quaternion.Euler(0, 0, 0);
            break;
         case PirateKind.TwoGuns:
            animator.SetLayerWeight((int)CharacterAnimatorLayers.CarryShortWeaponLeft, 0);
            animator.SetLayerWeight((int)CharacterAnimatorLayers.AbsoluteLayer, 1);
            animator.SetFloat(CharacterControllerParamKeys.FrontSpeed, 0.5f);
            animator.SetFloat(CharacterControllerParamKeys.Speed, 1.0f);
            transform.localScale = new Vector3(1, 1, 1);
            transform.rotation = Quaternion.Euler(0, 0, 0);
            break;
         case PirateKind.Captain:
            break;
      }
      _isEspecial = false;
   }

    public void Pulse()
    {
		pulse = !pulse;
		if (_kind == PirateKind.Captain)
		{
			NotifyPulse ();
		}
    }

	private void NotifyPulse()
	{
		if (pulse && TropaController.Instance.enabled) {
         TropaController.Instance.PlayVoice();
		} else{
		}
	}
}
