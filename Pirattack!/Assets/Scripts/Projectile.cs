﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
   [SerializeField]
   private GameObject _bloodParticles;

   public GameObject _explosion;

   [SerializeField]
   private Peloton _soldier;

   public GameObject _pirateTarget;

   public Pirata.PirateKind _characterWin;

   private bool p_checked = false;

   void Start()
   {
      this.gameObject.SetActive(false);
      this.enabled = false;
   }

	// Update is called once per frame
	void Update ()
   {
      if(!p_checked)
      {
         Check();
      }
      transform.position = Vector3.MoveTowards(transform.position, _pirateTarget.transform.position, 10 * Time.deltaTime);

      if(transform.position == _pirateTarget.transform.position)
      {
         this.enabled = false;
         //If my target is a pirate
         if (_pirateTarget.GetComponent<Pirata>() != null)
            FinishAttack();
         else
         {
            if (_explosion != null)
            {
               _explosion.SetActive(true);
               _soldier.Die();
            }
         }
      }
   }

   void Check()
   {
      //check matrix heroes/enemies
      Pirata p = _pirateTarget.GetComponent<Pirata>();
      this.enabled = false;

      //If we lost the confront
      if (p._kind != _characterWin || !p._isEspecial || p._kind == Pirata.PirateKind.Fat)
      {
         this.enabled = true;
      }
      else if( p._kind== Pirata.PirateKind.Woman && p._isEspecial)
      {
         p.GetComponentInChildren<Animator>().SetLayerWeight((int)CharacterAnimatorLayers.CarryLongWeapon, 1);
         p.GetComponentInChildren<Animator>().SetBool(CharacterControllerParamKeys.Fire, true);
         p.transform.GetChild(0).FindChild("Bola").gameObject.SetActive(true);
         _soldier.Die();
      }
      else if (p._kind == Pirata.PirateKind.TwoGuns && p._isEspecial)
      {
         p.GetComponentInChildren<Animator>().SetLayerWeight((int)CharacterAnimatorLayers.CarryShortWeaponLeft, 1);
         p.GetComponentInChildren<Animator>().SetBool(CharacterControllerParamKeys.Fire, true);
         p.transform.GetChild(0).FindChild("Bola").gameObject.SetActive(true);
         _soldier.Die();
      }
      else if (p._kind == Pirata.PirateKind.Bombs && p._isEspecial)
      {
         p.GetComponentInChildren<Animator>().SetLayerWeight((int)CharacterAnimatorLayers.CarryMeleeWeaponLeft, 1);
         p.GetComponentInChildren<Animator>().SetBool(CharacterControllerParamKeys.Fire, true);
         p.GetComponentInChildren<ProjectileChamaco>()._target = gameObject;
         p.GetComponentInChildren<ProjectileChamaco>().enabled = true;
         _soldier.Die();
      }

      p_checked = true;
   }

   void FinishAttack()
   {
      Pirata p = _pirateTarget.GetComponent<Pirata>();

      //If I am the fat pirate, block
      if(p._kind == Pirata.PirateKind.Fat && p._isEspecial)
      {
         _pirateTarget = transform.parent.gameObject;
         this.enabled = true;
         return;
      }

      p._salud = 0;
      if (p._salud > 0)
         return;

      p._isDead = true;

      //Animacion muerte
      p.transform.SetParent(transform);
      p.enabled = false;
      Animator animator = p.GetComponentInChildren<Animator>();
      animator.SetBool("idle", false);
      animator.SetBool("walking", false);
      animator.SetBool("signal1", false);
      animator.SetBool("dying", true);

      if(p._kind == Pirata.PirateKind.Woman ||p._kind == Pirata.PirateKind.TwoGuns)
      {
         animator.SetLayerWeight((int)CharacterAnimatorLayers.AbsoluteLayer, 1);
         animator.SetBool(CharacterControllerParamKeys.Death, true);
      }
      //If we kill the captain stop and fail text
      if (_pirateTarget.GetComponent<Pirata>()._kind == Pirata.PirateKind.Captain)
      {
         TropaController.Instance.enabled = false;
         UILevel.Instance.FailLevel();
      }
      _bloodParticles.SetActive(true);

      gameObject.GetComponent<MeshRenderer>().enabled = false;
   }
}
