﻿using UnityEngine;
using System.Collections;

public enum CharacterAnimatorLayers { Base = 0, Lateral = 1, OnAir = 2, Jumping = 3, CarryLongWeapon = 4, CarryShortWeaponLeft = 5, CarryShortWeaponRight = 6, CarryMeleeWeaponLeft = 7, AbsoluteLayer = 8 };

public class Peloton : MonoBehaviour
{
   public Animator[] Characteranimator;

   // Use this for initialization
   void Start ()
   {
      foreach (Animator a in Characteranimator)
      {
         a.SetLayerWeight((int)CharacterAnimatorLayers.CarryLongWeapon, 1);
         a.SetBool(CharacterControllerParamKeys.Aiming, true);
      }
   }
	
   public void Shot()
   {
      foreach (Animator a in Characteranimator)
      {
         a.SetLayerWeight((int)CharacterAnimatorLayers.CarryLongWeapon, 1);
         a.SetBool(CharacterControllerParamKeys.Fire, true);
      }
   }
   public void Die()
   {
      foreach (Animator a in Characteranimator)
      {
         a.SetLayerWeight((int)CharacterAnimatorLayers.AbsoluteLayer, 1);
         a.SetBool(CharacterControllerParamKeys.Death, true);
      }
   }

}
