﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tutorial : Singleton<Tutorial>
{

   public Image _black;
   public GameObject[] _bocadillos;
   public Animator _captainAnimator;
   public GameObject _acceptButton;
   public Animation _tripulationAnimation;
   public RythmVisual _white;
   public bool _active = true;
   public RythmGenerator rythm;
   public GameObject _audio1;
   public GameObject _audio2;

   private int p_index = 0;
   private float p_time = 0;

	// Use this for initialization
	void Awake ()
   {
      if (_black != null)
         _black.CrossFadeAlpha(0, 3, false);
      else
      {
         this.enabled = false;
         _active = false;
      }
      PlayerPrefs.SetInt("0", 3);
      PlayerPrefs.SetInt("1", 2);
      PlayerPrefs.SetInt("2", 1);
      PlayerPrefs.SetInt("3", 0);
   }
	
	// Update is called once per frame
	void Update ()
   {
      if (_black == null)
         return;

         p_time += Time.deltaTime;

      if(p_time >= 3 && p_index == 0)
      {
         _bocadillos[p_index++].SetActive(true);

         _captainAnimator.SetBool("idle", false);
         _captainAnimator.SetBool("walking", false);
         _captainAnimator.SetBool("dying", false);
         _captainAnimator.SetBool("signal1", true);
         p_time = 0;
      }
      else if(p_index == 1 && p_time >= 0.5f)
      {
         _captainAnimator.SetBool("idle", true);
         _captainAnimator.SetBool("walking", false);
         _captainAnimator.SetBool("dying", false);
         _captainAnimator.SetBool("signal1", false);
         p_index++;
         this.enabled = false;
         _acceptButton.SetActive(true);
      }
      else if(p_index == 2)
      {
         _bocadillos[0].SetActive(false);
         _bocadillos[1].SetActive(true);
         _tripulationAnimation.Play("Selected");
         this.enabled = false;
         _acceptButton.SetActive(true);
         p_index++;
      }
      else if(p_index == 3)
      {
         this.enabled = false;
         _acceptButton.SetActive(true);
         _bocadillos[1].SetActive(false);
         _bocadillos[2].SetActive(true);
         rythm.ComeOn();
         _white.enabled = true;
         TropaController.Instance.enabled = true;
         _tripulationAnimation.Stop("Selected");
         _tripulationAnimation.transform.localScale = new Vector3(1, 1, 1);
         p_index++;
      }
      else if(p_index == 4)
      {
         this.enabled = false;
         _bocadillos[2].SetActive(false);
         _bocadillos[3].SetActive(true);
         p_index++;
      }
      else if (p_index == 5)
      {
         this.enabled = false;
         _acceptButton.SetActive(true);
         _bocadillos[3].SetActive(false);
         _bocadillos[4].SetActive(true);
         p_index++;
         _active = false;
         StartCoroutine(Co_time());
      }
      else if (p_index == 6)
      {
         this.enabled = false;
         _bocadillos[4].SetActive(false);
         _bocadillos[5].SetActive(true);
         p_index++;
      }
      else if (p_index == 7)
      {
         _bocadillos[5].SetActive(false);
         this.enabled = false;
         StartCoroutine(Co_time2());
         p_index++;
      }
      else if (p_index == 8)
      {
         _bocadillos[6].SetActive(true);
         _acceptButton.SetActive(true);
         this.enabled = false;
         p_index++;
         _black.gameObject.SetActive(false);
      }
      else if (p_index == 9)
      {
         _bocadillos[6].SetActive(false);
         _bocadillos[7].SetActive(true);
         this.enabled = false;
         _audio1.gameObject.SetActive(false);
         _audio2.gameObject.SetActive(true);
         p_index++;
         StartCoroutine(Co_time3());
      }
      else if (p_index == 10)
      {
         _bocadillos[7].SetActive(false);
         _bocadillos[8].SetActive(true);
         this.enabled = false;
         p_index++;
         StartCoroutine(Co_time3());
      }
   }

   IEnumerator Co_time()
   {
      yield return new WaitForSeconds(6f);
      _active = true;
   }
   IEnumerator Co_time2()
   {
      yield return new WaitForSeconds(10f);
      this.enabled = true;
   }
   IEnumerator Co_time3()
   {
      yield return new WaitForSeconds(5f);
      this.enabled = true;
      _bocadillos[8].SetActive(false  );
   }

   public void Bubba()
   {
      _active = false;
      this.enabled = true;
   }
}
