﻿using UnityEngine;
using System.Collections;

public class InitialCameraEffect : MonoBehaviour
{
   [SerializeField]
   private GameObject _initialPoint;

   [SerializeField]
   private GameObject _finishPoint;

   [SerializeField]
   private GameObject _camera;

   [SerializeField]
   private GameObject _initialButton;

   [SerializeField]
   private AudioSource _audioTension;

   // Use this for initialization
   void Start ()
   {
      _camera.transform.position = _initialPoint.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
   {
      _camera.transform.position = Vector3.MoveTowards(_camera.transform.position, _finishPoint.transform.position, 50 * Time.deltaTime);

      if (_camera.transform.position.Equals(_finishPoint.transform.position))
      {
         _initialButton.SetActive(true);
         this.enabled = false;
      }
   }
}
