﻿using UnityEngine;
using System.Collections;

public class AudioRandom : MonoBehaviour
{

   public AudioSource _audioSource;

   public AudioClip[] _audios;

   float p_time = 0;

	// Update is called once per frame
	void Update ()
   {
      p_time += Time.deltaTime;

      if(p_time > 5)
      {
         _audioSource.clip = _audios[Random.Range(0, _audios.Length)];
         _audioSource.pitch = 1.0f - Random.Range(-0.1f, 0.1f);
         _audioSource.Play();

         p_time = 0;
      }
	}
}
