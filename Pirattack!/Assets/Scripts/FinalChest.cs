﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FinalChest : MonoBehaviour
{
	void OnTriggerEnter()
   {
      GetComponent<Animation>().Play();
      UILevel.Instance.WinLevel();
      PlayerPrefs.SetInt("level", SceneManager.GetActiveScene().buildIndex);
      TropaController.Instance.enabled = false;

      //Launch win animation
      foreach(Pirata p in TropaController.Instance._piratas)
      {
         p.GetComponentInChildren<Animator>().SetBool("idle", false);
         p.GetComponentInChildren<Animator>().SetBool("walking", false);
         p.GetComponentInChildren<Animator>().SetBool("signal1", true);
         p.GetComponentInChildren<Animator>().SetBool("dying", false);
      }
   }
}
