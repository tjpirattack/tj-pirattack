﻿using UnityEngine;
using System.Collections;

public class Piece : MonoBehaviour
{
	public enum TowersEnum{ Tower, cannon};

	[SerializeField]
	protected PuntoLista	_initialPoint;

	[SerializeField]
	protected PuntoLista	_finishPoint;

	[SerializeField]
	protected Piece		_nextPiece;


	// Use this for initialization
	void OnEnable ()
	{
		if (_nextPiece != null) {
			_nextPiece.transform.position = _finishPoint.gameObject.transform.position;
			_nextPiece.gameObject.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
