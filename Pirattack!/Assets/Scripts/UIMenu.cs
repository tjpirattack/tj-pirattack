﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
   [SerializeField]
   private Button[] _levelButtons;

   [SerializeField]
   private Image[] _characters;

   [SerializeField]
   private Sprite[] _characters2D;

   private int p_actualPirate;

   void Start()
   {
      PaintButtons();
   }

   public void PaintButtons()
   {
      for (int i = _levelButtons.Length - 1; i > PlayerPrefs.GetInt("level"); i--)
      {
         _levelButtons[i].image.color = Color.gray;
      }
   }

   public void ChangePirates(int i_pos)
   {
      p_actualPirate = i_pos;
   }

   public void SelectPirate(int i_pos)
   {
      PlayerPrefs.SetInt(p_actualPirate.ToString(), i_pos);
      _characters[p_actualPirate].sprite = _characters2D[i_pos];
   }

	public void LoadLevel(int i_level)
   {
      SceneManager.LoadScene(i_level);
   }

   public void ResetLevels()
   {
      PlayerPrefs.SetInt("level", 0);
      PaintButtons();
   }
}
