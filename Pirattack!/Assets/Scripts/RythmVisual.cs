﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RythmVisual : MonoBehaviour
{

   Image p_image;

	// Use this for initialization
	void Start ()
   {
      p_image = GetComponent<Image>();
      p_image.enabled = true;
      StartCoroutine(Co_Timer());
	}	

   IEnumerator Co_Timer()
   {
      while(true)
      {
         p_image.CrossFadeAlpha(1, 0.5f, false);
         yield return new WaitForSeconds(2f);
         p_image.CrossFadeAlpha(0, 0.5f, false);
         yield return new WaitForSeconds(2f);
      }
   }
}
