﻿using UnityEngine;
using System.Collections;

public class ProjectileChamaco : MonoBehaviour
{
   [SerializeField]
   private GameObject _bloodParticles;

   public GameObject _smog;

   public GameObject _target;
   
   void Start()
   {
      this.enabled = false;
      GetComponent<Renderer>().enabled = false;
   }

	// Update is called once per frame
	void Update ()
   {
      GetComponent<Renderer>().enabled = true;
      transform.position = Vector3.MoveTowards(transform.position, _target.transform.position, 10 * Time.deltaTime);

      if(transform.position == _target.transform.position)
      {
         _target.GetComponent<Projectile>()._explosion.SetActive(true);
         GetComponent<Renderer>().enabled = false;
         this.enabled = false;
      }
   }
}
